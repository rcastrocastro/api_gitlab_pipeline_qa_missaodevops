Dado("o endereço da API") do
  @headers = API_HEADERS
end

Quando("realizar a requisição GET") do
  @response = @service.call(RequisicaoAPI).find_ids
  @response.to_json
  p @response
end

Então("a API irá retornar os dados da consulta") do
  @result = @response[0]['id']
  p @result
end

Dado("o id da pipeline para consultar o status") do
  @id = @result
end

Quando("realizar a requisição GET com o id especifico da pipeline") do
  @response = @service.call(RequisicaoAPI).find_id_specific(@id)
  @response.to_json
  p @response
end

Então("na resposta da API deverá retornar o status {string}") do |expect_status|
  @result = @response[0]["status"]
  expect(@result).to eql expect_status
end