
# Classe de interface com a API
class RequisicaoAPI
    include HTTParty
    base_uri API_URI
    headers API_HEADERS
    default_timeout 120
  
    def initialize() end

    def find_ids
      self.class.get("/", headers: API_HEADERS)
    end
  
    def find_id_specific(id_pipe)
      self.class.get("/#{id_pipe}",
                     headers: API_HEADERS)
    end
  end