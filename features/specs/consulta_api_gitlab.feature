#language: pt

Funcionalidade: Consultar API da Pipeline
  Eu como usuário da Pipeline
  Gostaria de consultar o resultado da execução
  Para dar continuídade no deploy

  Cenário: Consultar IDs das pipelines
    Dado o endereço da API
    Quando realizar a requisição GET
    Então a API irá retornar os dados da consulta 

  Cenário: Consultar Status da pipeline
    Dado o id da pipeline para consultar o status
    Quando realizar a requisição GET com o id especifico da pipeline
    Então na resposta da API deverá retornar o status "success"